FlipByBlink-まばたき読書-プライバシーポリシー20181129

投稿日: 2018/11/29

FlipByBlink -まばたき読書- プライバシーポリシー

2018/11/29 作成

本app(FlipByBlink -まばたき読書-)ではappをインストールしたユーザーに対して以下の情報を収集します。
・Apple Inc.「TrueDepth API」
正確に述べると ARkit / ARFaceAnchor / blendShapes の情報にアクセスし、まばたきを検知しています。
https://developer.apple.com/documentation/arkit/arfaceanchor/2928251-blendshapes

アクセスした情報はページめくり機能に利用することのみを目的としてローカルでのみ処理を行います。アクセスした情報は永続的な保存処理は一切されず、また本app制作者や第三者に渡ることはありません。


source: https://web.archive.org/web/20191129034644/https://ie.u-ryukyu.ac.jp/e135753/2018/11/29/flipbyblink-まばたき読書-プライバシーポリシー20181129/
